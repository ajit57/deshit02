<?php
include "student.php";

//making a subclass
class Boy extends Student
{
	public $class= "B.Sc";
	public function play()
	{
		echo "He likes to play"."<br>";
		echo "He reads in ".$this->class;
	}
}

$b = new Boy();
$b->info('Kamrul');
$b->play();