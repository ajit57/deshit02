<?php

include '../vendor/autoload.php';

use App\Student;

$hobbies = implode(',', $_POST['hobbies']);
$_POST['hobbies'] = $hobbies;

$obj = new Student;
$obj->setData($_POST);
$singleData = $obj->edit();

$current_image = $singleData->image; 

if(!empty($_FILES['image']['name'])){
	$name = time().$_FILES['image']['name'];
	$tmplocation = $_FILES['image']['tmp_name'];
	$current_location = "../assets/images/".$name;
	move_uploaded_file($tmplocation, $current_location);	
	$_POST['image'] = $current_location;
}else{
	$_POST['image'] = $current_image;
}

$obj->setData($_POST);

$obj->update();