<?php
include '../vendor/autoload.php';
use App\Student;

$obj = new Student;
/*var_dump($_FILES['image']);
die();*/

$hobbies = implode(',', $_POST['hobbies']);
$_POST['hobbies'] = $hobbies;

$name = time().$_FILES['image']['name'];
$tmplocation = $_FILES['image']['tmp_name'];
$current_location = "../assets/images/".$name;
move_uploaded_file($tmplocation, $current_location);
$_POST['image'] = $current_location;

$obj->setData($_POST);
$obj->store();