<?php

namespace App;
use PDO;

class Database
{
	public $db;
	public $dbhost = "localhost";
	public $dbname = "crud-oop";
	public $dbuser = "root";
	public $dbpassword = "";

	public function __construct()
	{
		/*$this->db = new PDO('mysql:host=$this->dbhost;dbname=$this->dbname', $this->dbuser, $this->dbpassword);*/
		try {
            $this->db = new PDO("mysql:host=$this->dbhost;dbname=$this->dbname", $this->dbuser, $this->dbpassword);
            
            //echo "Database connected...";

        } catch (PDOException $error) {
            echo $error->getMessage();
        }

	}


}