<?php

namespace App;
use App\Database;
use PDO;

class Student extends Database
{
	protected $id;
	protected $name;
	protected $email;
	protected $course;
	protected $hobbies;
	protected $image;
	protected $address;

	public function setData($data){
		if(array_key_exists('id', $data))
			$this->id = $data['id'];

		if(array_key_exists('name', $data))
			$this->name = $data['name'];

		if(array_key_exists('email', $data))
			$this->email = $data['email'];

		if(array_key_exists('course', $data))
			$this->course = $data['course'];

		if(array_key_exists('hobbies', $data))
			$this->hobbies = $data['hobbies'];

		if(array_key_exists('image', $data))
			$this->image = $data['image'];

		if(array_key_exists('address', $data))
			$this->address = $data['address'];
	}

	public function index()
	{
		$query = "SELECT * from students";
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

	public function store()
	{
		$query = "INSERT into students (name, email, course, hobbies, image, address) Values( ?, ?, ?, ?, ?, ?)";
		$array_data = array($this->name, $this->email, $this->course, $this->hobbies, $this->image, $this->address);

		$statement= $this->db->prepare($query);

		$result = $statement->execute($array_data);
		if($result){
			echo "<script>alert('Student Created Successfully');location.href='../views/index.php'</script>";
		}else{
			echo "<script>alert('Student is not Created');location.href='../views/create.php'</script>";
		}

	}

	public function edit()
	{
		$query = "SELECT * from students where id=".$this->id;
		$sth= $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$singleData = $sth->fetch();
		return $singleData;
	}

	public function update()
	{
		$query = "UPDATE students set name=?, email=?, course=?, hobbies=?, image=?, address=? where id=".$this->id;
		$sth = $this->db->prepare($query);
		$array_data = array($this->name, $this->email, $this->course, $this->hobbies, $this->image, $this->address);
		$result = $sth->execute($array_data);
		if($result){
			echo "<script>alert('Student Updated Successfully');location.href='../views/index.php'</script>";
		}else{
			echo "<script>alert('Student is not Updated');location.href='../views/edit.php'</script>";
		}

	}

	public function delete()
	{
		$query = "DELETE from students where id=".$this->id;
		$result = $this->db->query($query);
		/*if($result){
			echo "<script>alert('Student Deleted Successfully');location.href='../views/index.php'</script>";
		}else{
			echo "<script>alert('Student is not Deleted');location.href='../views/index.php'</script>";
		}
*/	}

}






