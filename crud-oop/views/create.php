<?php include 'header.php'; ?>
	<div class="main-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<h3>Create Student <a href="index.php" class="btn btn-success pull-right">Back</a></h3>
					<form action="../app/store.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="name">Name</label>
							<input type="name" id="name" name="name" class="form-control">
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" id="email" name="email" class="form-control">
						</div>
						<div class="form-group">
							<label for="course">Select a course</label>
							<select name="course" id="course">
								<option value="c">C Programming</option>
								<option value="php">PHP Programming</option>
								<option value="java">Java Programming</option>
								<option value="python">Python Programming</option>
							</select>
						</div>
						<div class="form-group">
							<label for="hobbies">Select Hobbies</label>
							<input type="checkbox" value="playing" name="hobbies[]"> Playing
							<input type="checkbox" value="swimming" name="hobbies[]"> Swimming
							<input type="checkbox" value="drawing" name="hobbies[]"> Drawing
						</div>
						<div class="form-group">
							<label for="image">Select a file</label>
							<input type="file" name="image">
						</div>
						<div class="form-group">
							<label for="address">Address</label>
							<textarea name="address" id="address" cols="30" rows="5" class="form-control"></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	</div>
<?php include 'footer.php'; ?>