<?php

include "../vendor/autoload.php";
use App\Database;

new Database;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Simple Crud with OOP PHP</title>
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="text-center">Simple CRUD with OOP PHP</h1>
				</div>
			</div>
		</div>
	</header>
