<?php 
include 'header.php'; 
include '../vendor/autoload.php';


if(!isset($_SESSION) )session_start();

use App\User\User;
use App\User\Auth;
use App\Student;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    header("location:login.php");
}


$i= 1;

$obj = new Student;
$alldata = $obj->index();


?>
	<div class="main-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h4 class="text-right">Welcome <span style="color:green"><?php echo $_SESSION['email']; ?></span> <a class="btn btn-success" href="profile/logout.php">Logout</a></h4>
					<h3>Student List <a href="create.php" class="btn btn-success pull-right">Create</a></h3>
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Sl</th>
								<th>Name</th>
								<th>Email</th>
								<th>Course</th>
								<th>Hobbies</th>
								<th>Image</th>
								<th>Address</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($alldata as $data) { ?>
							<tr>
								<td><?php echo $i++; ?></td>
								<td><?php echo $data->name; ?></td>
								<td><?php echo $data->email; ?></td>
								<td><?php echo $data->course; ?></td>
								<td><?php echo $data->hobbies; ?></td>
								<td><img src="<?php echo $data->image; ?>" class="img-responsive" width="50"></td>
								<td><?php echo $data->address; ?></td>
								<td>
									<a href="edit.php?id=<?php echo $data->id; ?>" class="btn btn-warning btn-sm">Edit</a>
									<a href="../app/delete.php?id=<?php echo $data->id; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure!')">Delete</a>
								</td>
							</tr>

							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php include 'footer.php'; ?>