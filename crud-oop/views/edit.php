<?php 
include 'header.php'; 
include '../vendor/autoload.php';
use App\Student;
$obj= new Student;
$obj->setData($_GET);
$singleData = $obj->edit();

$hobbies = explode(',', $singleData->hobbies);


?>
	<div class="main-content">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<h3>Edit Student <a href="index.php" class="btn btn-success pull-right">Back</a></h3>
					<form action="../app/update.php" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?php echo $singleData->id; ?>">
						<div class="form-group">
							<label for="name">Name</label>
							<input type="name" value="<?php echo $singleData->name; ?>" id="name" name="name" class="form-control">
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" value="<?php echo $singleData->email; ?>" id="email" name="email" class="form-control">
						</div>
						<div class="form-group">
							<label for="course">Select a course</label>
							<select name="course" id="course">
								<option value="c" <?php if($singleData->course == 'c') echo "selected"; ?>>C Programming</option>
								<option value="php" <?php if($singleData->course == 'php') echo "selected"; ?>>PHP Programming</option>
								<option value="java" <?php if($singleData->course == 'java') echo "selected"; ?>>Java Programming</option>
								<option value="python">Python Programming</option>
							</select>
						</div>
						<div class="form-group">
							<label for="hobbies">Select Hobbies</label>
							<input type="checkbox" value="playing" name="hobbies[]" <?php if(in_array('playing', $hobbies)) echo "checked"; ?>> Playing
							<input type="checkbox" value="swimming" name="hobbies[]" <?php if(in_array('swimming', $hobbies)) echo "checked"; ?>> Swimming
							<input type="checkbox" value="drawing" name="hobbies[]" <?php if(in_array('drawing', $hobbies)) echo "checked"; ?>> Drawing
						</div>
						<div class="form-group">
							<label for="image">Select a file</label>
							<input type="file" name="image">
						</div>
						<div class="form-group">
							<label for="address">Address</label>
							<textarea name="address" id="address" cols="30" rows="5" class="form-control"><?php echo $singleData->address; ?></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Update</button>
					</form>
				</div>
				<div class="col-sm-3">
					<img src="<?php echo $singleData->image; ?>" class="img-responsive" alt="">
				</div>
			</div>
		</div>
	</div>
<?php include 'footer.php'; ?>