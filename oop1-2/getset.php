<?php


class Property
{
	public $name = "AB Bank";

	public function getProperty()
	{
		echo "I am the owner of ".$this->name."<br>";	
	}

	public function setProperty($name)
	{
		$this->name = $name;
	}

}

$kamal = new Property();
$kamal->getProperty();
$kamal->setProperty('Grameen Bank');
$kamal->getProperty();
$kamal->setProperty('City Bank');
$kamal->getProperty();