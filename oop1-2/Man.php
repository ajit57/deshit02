<?php

class Man
{
	public $name= "Admin";//property
	public $country;
	protected $property = "A nice Building"; 
	private $inbox = "Some private message";

	public function info()//method
	{
		echo "He is from world"."<br>";
	}
	public function nationality($country)
	{
      $this->country = $country;
      echo "He is ".$this->country."<br>";
	}
	protected function bankinfo()
	{
		echo "My Bank Details.";
	}
	private function visiting()
	{
		echo "I am going to Coxs Bazar";
	}
	public function getProperty()
	{
		echo $this->property."<br>";
		$this->bankinfo();
	}
	public function getPrivate()
	{
		echo $this->inbox."<br>";
		$this->visiting();
	}
}

$a = new Man;
echo $a->name."<br>";
$a->info();
$a->nationality('Bangladeshi');
$a->getProperty();
//echo $a->inbox."<br>";
$a->getPrivate();
