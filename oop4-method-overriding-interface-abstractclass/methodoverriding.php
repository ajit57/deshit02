<?php


class Dept
{
	public $name;
	public $dean;
	public $session;

	public function __construct($name, $dean, $session)
	{
		$this->name = $name;
		$this->dean = $dean;
		$this->session = $session;
	}

	public function activity(){
		echo $this->name. " Department have some good student in session ".$this->session."<br>";
	}
}

class CseDept extends Dept
{
	public function activity()
	{
		parent::activity();
		echo $this->name." Department arrange a competition every year";
	}
}
$obj = new Dept('Physics', 'Mr. X', 2016);
$obj->activity();

$obj1 = new CseDept('Cse', 'Mr. Javed', 2013);
$obj1->activity();

