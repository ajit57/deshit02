<?php
interface abc 
{
	public function play();
}
interface cd 
{
	public function sing();
}
interface abcd extends abc, cd 
{
	public function swim();
}

class Human implements abcd
{
	public function play()
	{
		echo "Human Can play";
	}
	public function sing()
	{
		echo "Human can sing";
	}
	public function swim()
	{
		echo "Human can swim";
	}
}
$obj = new Human;
$obj->play();