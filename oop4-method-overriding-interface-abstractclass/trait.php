<?php

trait World
{
	public function move(){
		echo "The World is moving round the sun."."<br>";
	}
}
trait Sun 
{
	public function shine()
	{
		echo "The sun shines brightly";
	}
} 
class Mars
{
	public function info()
	{
		echo "Mars is Planet"."<br>";
	}
}
class Moon extends Mars
{
	use World, Sun;
}
$obj = new Moon;
$obj->move();
$obj->info();
$obj->shine();
