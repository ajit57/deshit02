<?php

namespace Physics;

class Student
{
	public $name = "Nizam";

	public function info()
	{
		echo "Nizam Reads in Physics.";
	}
}

class Boy
{
	public $name = "Saiful";

	public function info()
	{
		echo "Saiful is not a student";
	}
}