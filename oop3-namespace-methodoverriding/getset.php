<?php

class Animal
{
	public $mypets= [];

	public $firstpet = "Cat";

	public function __set($name, $value)
	{
		$this->mypets[$name] = $value;
	}
	public function __get($name)
	{
		return $this->mypets[$name];
	}
}
$obj = new Animal;
$obj->next = "Dog";
echo $obj->next;
echo "<br>";
$obj->another = "Parrot";
echo $obj->another;
echo "<br>";
$obj->last = "Tiger";
echo $obj->last;
echo "<br>";
var_dump($obj->mypets);