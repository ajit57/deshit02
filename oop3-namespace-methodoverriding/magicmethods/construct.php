<?php


class People
{
	public function __destruct()
	{
		echo "Object building completed";
	}

	public function __construct()
	{
		//$this->name = $name;
		echo "Start building object......."."<br>";
		//echo "My name is ".$this->name;	
	}

	public function details()
	{
		echo "He is a general public"."<br>";
	}

	
}

class Vip extends People
{
	public function __construct(){
		//parent::__construct();
		//echo "SubObject Building starting";
	}
}

/*$obj = new People();
$obj->details();
echo "<br>";
*/
$obj2 = new Vip;