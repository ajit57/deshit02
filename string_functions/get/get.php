<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Capture with Get Method</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<h1>Form with Capture file</h1>
				<form action="captureget.php" method="GET" class="form">
					<div class="form-group">
						<label for="name">Your Name</label>
						<input type="text" id="name"
						name="name" class="form-control">
					</div>
					<div class="form-group">
						<label for="email">Your Email</label>
						<input type="email" id="email"
						name="email" class="form-control">
					</div>
					<button type="Submit" name="submit" class="btn btn-success">Submit</button>
				</form>
			</div>
		</div>
	</div>
	
</body>
</html>