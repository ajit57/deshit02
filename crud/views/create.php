<?php include "header.php"; ?>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<h2>Create Form <a href="index.php" class="btn btn-primary pull-right">Back</a></h2>
					<form action="../app/store.php" method="post">
						<div class="form-group">
							<label for="name">Name</label>
							<input type="name" name="name" id="name" placeholder="Enter your name" class="form-control">
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" name="email" id="email" placeholder="Enter your email" class="form-control">
						</div>
						<div class="form-group">
							<label for="address">Address</label>
							<textarea name="address" id="address" cols="30" rows="5" class="form-control"></textarea>
						</div>
						<div class="form-group">
							<label for="gender">Select Your Gender</label>
							<input type="radio" name="gender" value="male"> Male
							<input type="radio" name="gender" value="female"> Female
							<input type="radio" name="gender" value="other"> Other
						</div>
						<div class="form-group">
							<label for="mobile">Mobile</label>
							<input type="number" name="mobile" id="mobile" placeholder="Enter your mobile" class="form-control">
						</div>
						<button type="submit" name="submit" class="btn btn-success">Create</button>
					</form>
					
				</div>
			</div>
		</div>
	</section>

<?php include "footer.php"; ?>