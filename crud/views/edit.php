<?php 
if(isset($_GET['id'])){

include "header.php"; 

$id = $_GET['id'];
$query = $db->query("select * from profiles where id='$id' LIMIT 1");
$profile = $query->FETCH_ASSOC();
//var_dump($profile);
?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<h2>Edit Form <a href="index.php" class="btn btn-primary pull-right">Back</a></h2>
					<form action="../app/update.php" method="post">
						<input type="hidden" name="id" value="<?php echo $profile['id']; ?>">
						<div class="form-group">
							<label for="name">Name</label>
							<input type="name" name="name" id="name" placeholder="Enter your name" value="<?php echo $profile['name']; ?>" class="form-control">
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" name="email" id="email" placeholder="Enter your email"  value="<?php echo $profile['email']; ?>" class="form-control">
						</div>
						<div class="form-group">
							<label for="address">Address</label>
							<textarea name="address" id="address" cols="30" rows="5" class="form-control"><?php echo $profile['address']; ?></textarea>
						</div>
						<div class="form-group">
							<label for="gender">Select Your Gender</label>
							<input type="radio" name="gender" value="male" <?php if($profile['gender'] == 'male') echo 'checked' ?>> Male
							<input type="radio" name="gender" value="female" <?php if($profile['gender'] == 'female') echo 'checked' ?>> Female
							<input type="radio" name="gender" value="other" <?php if($profile['gender'] == 'other') echo 'checked' ?>> Other
						</div>
						<div class="form-group">
							<label for="mobile">Mobile</label>
							<input type="number" name="mobile" id="mobile" value="<?php echo $profile['mobile']; ?>" placeholder="Enter your mobile" class="form-control">
						</div>
						<button type="submit" name="submit" class="btn btn-success">Update</button>
					</form>		
				</div>
			</div>
		</div>
	</section>
<?php 
include "footer.php"; 

}

?>