<?php include "header.php"; ?>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2>Profile List <a href="create.php" class="btn btn-primary pull-right">Create</a></h2>
					
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Sl</th>
								<th>Name</th>
								<th>Email</th>
								<th>Address</th>
								<th>Gender</th>
								<th>Mobile</th>
								<th>Action</th>
							</tr>
						</thead>

						<tbody>
					<?php
						$query = $db->query("select * from profiles");
						$i=1;
						while($profile = $query->fetch_assoc()){
					?>
							<tr>
								<td><?php echo $i++; ?></td>
								<td><?php echo $profile['name']; ?></td>
								<td><?php echo $profile['email']; ?></td>
								<td><?php echo $profile['address']; ?></td>
								<td><?php echo $profile['gender']; ?></td>
								<td><?php echo $profile['mobile']; ?></td>
								<td>
									<a href="edit.php?id=<?php echo $profile['id']; ?>" class="btn btn-warning btn-sm">Edit</a>
									<a href="delete.php?id=<?php echo $profile['id']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
								</td>
							</tr>
					<?php
						}
					?>
						</tbody>
					</table>
					
				</div>
			</div>
		</div>
	</section>

<?php include "footer.php"; ?>