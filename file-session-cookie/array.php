<?php

$fruits = ['banana', 'mango', 'pineapple'];

var_dump($fruits);

echo "<br>";
echo "<br>";

echo $fruits['1'];

echo "<br>";
echo "<br>";

$info = [
	"name" => "Ajit",
	"age" => 234,
	"address" => "ctg",
	"student" =>[
		"class" => "Two",
		"sub" => [
			"major" => "English",
			"non-major" => "Math"
		]
	]
];

var_dump($info);
echo "<br>";
echo "<br>";
echo 'My name is '.$info['name'];
echo "<br>";
echo "<br>";
echo "I read in ".$info['student']['class'];

echo "<br>";
echo "<br>";

echo "My major subject is ".$info['student']['sub']['major'];